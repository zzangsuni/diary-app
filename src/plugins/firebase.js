import firebase from 'firebase/firebase-app';
import auth from 'firebase/firebase-auth';
import database from 'firebase/firebase-database';
import storage from 'firebase/firebase-storage';
export default {firebase, auth, database, storage};